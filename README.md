# Statistics Stuff

## K_Factors.R
Code to generate one- and two-sided k-factor tables.

## Binomial_Detection.R
Code to generate a table showing the detectability of defects given a population defect level and sample size.